package be.kdg.java2.flightbooking.repository;

import be.kdg.java2.flightbooking.domain.PaymentInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayementInfoRepository extends JpaRepository<PaymentInfo, Long> {
}
