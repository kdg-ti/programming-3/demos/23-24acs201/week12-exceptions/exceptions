package be.kdg.java2.flightbooking.repository;

import be.kdg.java2.flightbooking.domain.PassengerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerInfoRepository extends JpaRepository<PassengerInfo, Long> {
}
