package be.kdg.java2.flightbooking.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
public class DestinationNotAvailableException extends RuntimeException {
    public DestinationNotAvailableException(String message) {
        super(message);
    }
}
