package be.kdg.java2.flightbooking.ui;

import be.kdg.java2.flightbooking.exceptions.InsufficientMoneyException;
import be.kdg.java2.flightbooking.exceptions.InvalidAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class ExceptionHandlerController {
	private static Logger LOG = LoggerFactory.getLogger(ExceptionHandlerController.class);

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(InsufficientMoneyException.class)
	public String handleAccountException(Exception e, Model model){
		LOG.error("Error in exceptionHandler" +e);
		model.addAttribute("error", "You can't go that far");
		return "error/payment-error";
	}
}
