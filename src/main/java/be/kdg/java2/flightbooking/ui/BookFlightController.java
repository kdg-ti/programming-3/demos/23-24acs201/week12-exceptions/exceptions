package be.kdg.java2.flightbooking.ui;

import be.kdg.java2.flightbooking.exceptions.DestinationNotAvailableException;
import be.kdg.java2.flightbooking.exceptions.InvalidAccountException;
import be.kdg.java2.flightbooking.service.FlightBookingService;
import be.kdg.java2.flightbooking.ui.viewmodels.FlightBookingViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class BookFlightController {
    private FlightBookingService flightBookingService;
    private static Logger LOG = LoggerFactory.getLogger(BookFlightController.class);

    public BookFlightController(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @GetMapping({"","/index","/"})
    public String getBookFlightPage(Model model){
        model.addAttribute("destinations", flightBookingService.getDestinations());
        model.addAttribute("flightBooking",new FlightBookingViewModel());
        return "index";
    }

    @PostMapping("/bookflight")
    public String bookFlight(FlightBookingViewModel flightBookingViewModel){
        flightBookingService.bookFlight(flightBookingViewModel.getName(), flightBookingViewModel.getDestination(), flightBookingViewModel.getAccount());
        return "redirect:index";
    }

    // logic in ctrl method to show error page on exception
    public String bookFlightWithErrorPage(FlightBookingViewModel flightBookingViewModel){
        try {
            flightBookingService.bookFlight(flightBookingViewModel.getName(), flightBookingViewModel.getDestination(), flightBookingViewModel.getAccount());
        } catch (DestinationNotAvailableException e) {
            LOG.error(e.getMessage());
            return "error/destination-not-available";
        }
        return "redirect:index";
    }

    @ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
    @ExceptionHandler(InvalidAccountException.class)
    public String handleAccountException(Exception e,Model model){
        LOG.error("Error in exceptionHandler" +e);
        model.addAttribute("error", "Something happened to me on my way th the forum");
        return "error/account-error";
    }
}
